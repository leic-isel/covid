package pt.isel.poo.covid.view;

import pt.isel.poo.covid.CovidController;
import pt.isel.poo.covid.R;
import pt.isel.poo.tile.Img;

public class TrashView extends ElementView {

    /**
     * Construction  of a thrash object
     * @param ctx - Context of the application.
     */
    public TrashView(CovidController ctx) {
        img = new Img(ctx, R.drawable.trash);
    }
}
