package pt.isel.poo.covid.model;

public class Position {
    public final int line;
    public final int col;

    private static Position[][] all;

    /**
     * Constructor
     * Set to private due to be used only by itself.
     * @param l - Vertical position
     * @param c - Horizontal position
     */
    private Position(int l, int c) {
        line = l;
        col = c;
    }

    /**
     * Position initialization.
     * Static method, because it is going to be used by all objects.
     *
     * @param lines - Vertical position
     * @param cols - Horizontal position
     */
    static void init(int lines, int cols) {
        all = new Position[lines][cols];
    }

    /**
     * Evaluates the need to create a new position or use the same that already exists.
     * @param l - Vertical position
     * @param c - Horizontal position
     * @return Position - Object of type Position.
     */
    public static Position of(int l, int c) {
        Position pos = all[l][c];
        if(pos==null)
            all[l][c] = pos = new Position(l, c);
        return pos;
    }

    /**
     * The use of this method, allows to add a new position, but only after it has been
     * verified if the new position is within limits of the game board.
     * @param dir - Direction reference
     * @return - New Position
     */
    public Position add(Dir dir) {
        int l = line+dir.dl, c = col+dir.dc;
        if (l < 0 || l >= all.length) l = line;
        if (c < 0 || c >= all[0].length) c = col;
        return Position.of(l, c);
    }
}
