package pt.isel.poo.covid.model;

import androidx.annotation.NonNull;
import java.util.Iterator;
import java.util.LinkedList;

public class Arena implements Iterable<Element>{

    //Map of Elements
    private Element[][] map;

    //Constructor
    public Arena(int height, int width) {
        map = new Element[height][width];
    }

    /**
     * Allows to insert an element in a determined position, by grabbing the
     * coordinates directly from the location inside the element position.
     *
     * @param elem - Element type.
     */
    void set (Element elem) {
        map[elem.position.line][elem.position.col] = elem;
    }

    /**
     * Reads the contents in the map of elements, in a position passed by parameter
     * returning the element present there or null if there isn't an element there.
     *
     * @param pos - Object of type position.
     * @return - The element in the aforementioned position, if exists.
     */
    public Element get(Position pos) {
        return map[pos.line][pos.col];
    }

    /**
     * Return the length of the array that corresponds to the height of the map.
     *
     * @return - The value containing the height of the map of elements.
     */
    int getHeight() {
        return map.length;
    }

    /**
     * Return the length of the array that corresponds to the width of the map.
     *
     * @return - The value containing the width of the map of elements.
     */
    int getWidth() {
        return map[0].length;
    }

    /**
     * Allows to delete a content of a position in the map of elements, by setting
     * its value to null.
     *
     * @param p - Position of the element.
     */
    public void resetPosition(Position p) {
        map[p.line][p.col] = null;
    }

    /**
     * Reset the full content in the map of elements by setting all values to null.
     *
     */
    public void reset(){
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[0].length; j++) {
                map[i][j] = null;
            }
        }

    }

    /**
     * Analyzes every element inside the map of elements, and verifies if it is
     * a movable element and adds it to a list of movable objects.
     *
     * @return List of movable elements.
     */
    LinkedList<Element> removeMovables() {
        LinkedList<Element> lst = new LinkedList<>();
        for (Element elem : this) {
            if (elem.isMovable()) {
                lst.add(elem);
            }
        }
        return lst;
    }

    @NonNull
    @Override
    public Iterator<Element> iterator() {
        return new Iterator<Element>() {
            private Position cur = getFirst();
            @Override
            public boolean hasNext() {
                return cur!=null;
            }
            @Override
            public Element next() {
                Element e = get(cur);
                cur = getNext(cur);
                return e;
            }
        };
    }

    /**
     * Obtains the first elements from the map of elements.
     *
     * @return - Position for the first element.
     */
    private Position getFirst() {
        for (Element[] elem : map) {
            for (Element a : elem) {
                if (a != null) return a.position;
            }
        }
        return  null;
    }

    /**
     * Verifies if there is a next position in the map of elements.
     *
     * @return - Element position.
     */
    private Position getNext(Position cur) {
        int line = cur.line;
        int col = cur.col+1;
        if (col >= map[0].length) {
            line++;
            col=0;
        }
        for (int l = line; l < map.length; ++l) {
            for (int c = (l==line)?col:0; c < map[0].length; ++c) {
                Element e = map[l][c];
                if (e!=null) return e.position;
            }
        }
        return null;
    }

}
