package pt.isel.poo.covid.model;

public class Level {

    private int levelNumber;
    char[][] charMap;

    // Mandatory constructor declared in class Loader
    public Level(int levelNumber, int height, int width) {
        this.levelNumber = levelNumber;
        charMap = new char[height][width];
    }
    // mandatory methods declared in class Loader

    /**
     * Returns the current level number.
     *
     * @return Level number.
     */
    public int getNumbLevel() {
        return levelNumber;
    }

    /**
     * Reset the game to level 1
     */
    public void reset() { levelNumber = 1; }
    // mandatory methods declared in class Loader
    public void put(int l, int c, char type) {
        charMap[l][c] = type;
    }
}
