package pt.isel.poo.covid.model;

/**
 * Class that manages the direction of movements ingame.
 */
public enum Dir {
    LEFT(0,-1), RIGHT(0,+1), DOWN(+1, 0) /*, UP(-1, 0)*/ ;
    final int dl;
    final int dc;

    /**
     * Updates the current direction.
     * @param dl - If value different from 0, then move is vertically
     * @param dc - If value different from 0, the move is horizontally
     */
    Dir(int dl, int dc) {
        this.dl = dl;
        this.dc = dc;
    }
}
