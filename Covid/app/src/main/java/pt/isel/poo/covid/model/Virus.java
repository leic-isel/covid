package pt.isel.poo.covid.model;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Virus extends Element {

    Virus(int l, int c, CovidModel m) {
        super(l, c, m);
    }

    /**
     * Required constructor if there is the need to use reflection on this class.
     */
    Virus() {}

    /**
     * Request to move to a certain direction.
     *
     * @param dir - Direction to be moved.
     */
    void move(Dir dir) {
        super.move(dir);
    }

    /**
     * Verifies if there was a collision between the virus and some other element,
     * movable or not.
     * @param old - Position reference
     * @return - true if there was a collision with another element.
     */
    @Override
    boolean elementColision(Position old) {
        if (model.currDir != Dir.DOWN){
            move(model.currDir);
            return true;
        }
        return false;
    }

    /**
     * Informs if the object in question is movable.
     *
     * @return - For this object is always true.
     */
    @Override
    boolean isMovable() {
        return true;
    }

    /**
     * It allows to verify if the virus collided with the trash can.
     * If true, updates the flag virusnumb and requests the update of the view,
     * and the removal from the map of elements.
     *
     * @param p - Future position.
     */
    @Override
    void trashCollision(Position p) {
        model.destroyElement(p);
        model.arena.resetPosition(p);
    }

    /**
     * Verifies if an element has been moved, and returns true if there was movement
     *
     * @param old - Position type referring to the old position
     * @return - Return true if movement was successful
     */
    @Override
    boolean isMove(Position old) {
        return super.isMove(old);
    }

    /**
     * Saves the current state of the element.
     *
     * @param dos - Stream of data
     * @throws IOException - Generic class of exceptions for input/output information.
     */
    @Override
    void save(DataOutputStream dos) throws IOException {
        super.save(dos);
    }

    /**
     * Reads from the output stream information regarding the position of an element.
     *
     * @param dis - Stream of data
     * @throws IOException - Generic class of exceptions for input/output information.
     */
    @Override
    void load(DataInputStream dis) throws IOException {
        super.load(dis);
    }

}
