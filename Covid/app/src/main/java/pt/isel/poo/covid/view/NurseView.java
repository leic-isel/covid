package pt.isel.poo.covid.view;

import android.graphics.Canvas;
import pt.isel.poo.covid.CovidController;
import pt.isel.poo.covid.R;
import pt.isel.poo.tile.Img;

public class NurseView extends ElementView {
    private Img dead;
    private CovidController ctx;
    private boolean isDead = false;

    /**
     * Construction  of a nurse view object. Also instantiates the skull view object, in case
     * of death of the nurse.
     *
     * @param ctx - Context of the application.
     */
    public NurseView(CovidController ctx){
        img = new Img(ctx, R.drawable.nurse);
        dead = new Img(ctx, R.drawable.dead);
        this.ctx = ctx;
    }

    /**
     * Allows to set the nurse has dead.
     * @param loser
     */
    public void setDeadNurse(boolean loser) {
        isDead = loser;
    }

    /**
     * Overrides the draw method from the parent class, due to the evaluation if the nurse
     * is dead or not.
     * In case it is true, it will draw a skull image over the nurse image.
     * @param canvas Canvas
     * @param side
     */
    @Override
    public void draw(Canvas canvas, int side) {
        super.draw(canvas, side);
        if(isDead) {
            dead.draw(canvas, side, side, p);
        }
    }
}
