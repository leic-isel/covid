package pt.isel.poo.covid.view;

import pt.isel.poo.covid.CovidController;
import pt.isel.poo.covid.model.CovidModel;
import pt.isel.poo.covid.model.Element;
import pt.isel.poo.covid.model.GameListener;
import pt.isel.poo.covid.model.Position;
import pt.isel.poo.tile.Tile;
import pt.isel.poo.tile.TilePanel;

public class CovidView implements GameListener {

    private TilePanel panel;
    private CovidModel model;
    private NurseView nurseView;

    /**
     * Constructor for the graphic part of the application.
     * Sets the visualization by receiving the information about the dimension of the
     * gameboard, and drawing the board with the use of the tilepanel class.
     * Sets the listener, to be warned about the update request from the controller.
     * Also creates an HashMap to have the elements of the game.
     *
     * @param ctx - Context of the application
     * @param model - Object referencing the model
     * @param panel - Object referencing the tilepanel
     */
    public CovidView(CovidController ctx, CovidModel model, TilePanel panel) {
        this.model = model;
        initTilePanel(panel, model);
        model.addListener(this);
        ElementView.createHashMap(ctx);
    }

    /**
     * This method has the responsibility to create the layout size of the appplicaton, by using
     * using information sent from the model.
     *
     * @param panel - Object referencing the tilepanel
     * @param model - Object referencing the model.
     */
    public void initTilePanel (TilePanel panel, CovidModel model) {
        this.panel = panel;
        int l = model.getLines();
        int c = model.getCols();
        panel.setSize(c, l);
    }

    /**
     * Allow to create an element on screen.
     *
     * @param elem - Element object
     */

    @Override
    public void elementCreated(Element elem) {
        Tile tile = ElementView.getInstance(elem);
        String name = elem.getClass().getSimpleName();
        if(name.equals("Nurse"))
            nurseView = (NurseView) tile;
        panel.setTile(elem.getPosition().col, elem.getPosition().line, tile);
    }

    /**
     * This method reacts to movement, and has the sole responsibility to remove the element
     * from the previous position, by setting the position to null.
     *
     * @param elem - Element object
     * @param oldPos - Position of the element.
     */
    @Override
    public void elementMoved(Element elem, Position oldPos) {
        Tile t = getTile(oldPos);
        setTile(t, elem.getPosition());
        setTile(null, oldPos);
    }

    /**
     * Allows to remove an element from the screen.
     *
     * @param p - Position of the element.
     */
    @Override
    public void elementDestroy(Position p) {
        panel.setTile(p.col, p.line, null);
    }

    /**
     * Updates the position of an element to a new position on the screen
     *
     * @param pos - Position of the element.
     */
    @Override
    public void elementUpdate(Position pos) {
        Tile t = getTile(pos);
        setTile(t, pos);
    }

    /**
     * If the nurse is dead, then set the flag to true.
     * On the next opportunity that exists, the image will be overlaid with a skull.
     *
     * @param loser - Boolean value, if true then the nurse is dead.
     */
    @Override
    public void nurseDead(boolean loser) {
        nurseView.setDeadNurse(loser);
    }

    /**
     * Obtains if there is any element in a position.
     * @param p - Position of the element
     * @return - If exists, returns an element, or null if it doesn't exist.
     */
    private ElementView getTile(Position p){
        return (ElementView) panel.getTile(p.col, p.line);
    }

    /**
     * Sets a new tile in a determined position.
     * @param t - Tile
     * @param p - Position
     */
    private void  setTile(Tile t, Position p) {
        panel.setTile(p.col, p.line, t);
    }

    /**
     * Reset all the screen, by setting it to null. If nothing is draw, only the background
     * color will be at that spot.
     */
    public void reset() {
        int line = model.getLines();
        int col = model.getCols();

        for (int l = 0; l < line; l++) {
            for (int c = 0; c < col; c++) {
                panel.setTile(c, l, null);
            }
        }
    }

}
