package pt.isel.poo.covid.model;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Class the manages all types of elements ingame.
 */
public abstract class Element {

    //Field is "protected" due to the need of it's use by the classes that are extending it.
    protected Position position;

    CovidModel model;

    //Constructor.
    public Element(int l, int c, CovidModel model) {
        position = Position.of(l, c);
        this.model = model;
        model.createElement(this);
    }

    //Null Constructor.
    Element() {}

    /**
     * Requests the current position for the element.
     * @return - Position information for the element.
     */
    public Position getPosition() {
        return position;
    }

    /**
     * Request a move to a determine position by receiving the name of the direction, set in
     * Dir class, inside an Enumerator.
     *
     * @param dir - Direction to be moved.
     */
    void move(Dir dir) {
        move(position.add(dir));
    }

    /**
     * Request a move by passing an intended position.
     * This request requires that the old position is saved and passes both positions,
     * to a method that evaluates if it is possible to complete the request.
     * @param p - Position to be moved.
     */
    void move (Position p) {
        Position old = position;
        position = p;
        model.movedElement(old, this);
    }

    /**
     * Generic method that allows to detect element collisions
     * The position that is verified, by calling this method, is the future position for the
     * the element. This method is executed before the completion of the movement.
     * To be implemented inside all classes that extend Element
     *
     * @param pos - Position reference
     *
     * @return - Informs if element can move to that position or not.
     */
    abstract boolean elementColision(Position pos);

    /**
     * Generic method that allows to determine if an element is movable or not.
     * To be implemented inside all classes that extend Element
     *
     * @return - Returns if an element is movable or not.
     */
    abstract boolean isMovable();

    /**
     * Generic method that allows to determine if something as fallen inside the garbage can.
     * To be implemented inside all classes that extend Element
     *
     * @param p - Future position.
     */
    abstract void trashCollision(Position p);

    /**
     * Verifies if an elements has been moved, and returns true if there was movement
     *
     * @param old - Position type referring to the old position
     * @return - Return true if movement was successful
     */
    boolean isMove(Position old){
        return Math.abs(this.position.col - old.col) > 1;
    }

    /**
     * Write to the output stream information about the position of an element.
     *
     * @param dos - Stream of data
     * @throws IOException - Generic class of exceptions for input/output information.
     */
    void save(DataOutputStream dos) throws IOException {
        dos.writeByte(position.line);
        dos.writeByte(position.col);
    }

    /**
     * Reads from the output stream information regarding the position of an element.
     *
     * @param dis Stream of data
     * @throws IOException - Generic class of exceptions for input/output information.
     */
    void load(DataInputStream dis) throws IOException {
        position = Position.of(dis.readByte(), dis.readByte());
    }
}
