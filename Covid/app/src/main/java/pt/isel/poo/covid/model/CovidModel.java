package pt.isel.poo.covid.model;

import androidx.annotation.NonNull;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;

public class CovidModel implements Iterable<Element>{
    private int numVirus=0;
    private Loader loader;
    private Level level;
    protected Dir currDir;

    protected Arena arena;
    private Nurse nurse;

    //Checks the status of the game
    private boolean endGame = false;

    /**
     * Constructor of the game model.
     * Receives information about the levels and level number and initializes the creation the
     * the level.
     *
     * @param in - Information from file containing the description of the levels.
     * @param levelNumb - Number of the level to be created.
     *
     * @throws Loader.LevelFormatException - If the information for the level construction
     *                                      format isn't correct, it generates an exception.
     */
    public CovidModel(Scanner in, int levelNumb) throws Loader.LevelFormatException {
        setLoader(in);
        initLevel(levelNumb);
        initArena();
    }

    /**
     * Creates an object of type loader with the information to create the level.
     *
     * @param in - Information from the file containing the description of the levels.
     */
    public void setLoader(Scanner in) {
        loader = new Loader(in);
    }

    /**
     * Verifies if the endGame field if true.
     *
     * @return the current status of field endGame.
     */
    public boolean getEndGame() {
        return endGame;
    }

    /**
     * Allows to modify the field of endGame.
     *
     * @param endGame - Receives a boolean to set the status of the endGame field.
     */
    public void setEndGame(boolean endGame) {
        this.endGame = endGame;
    }

    /**
     * Creates the level object, by loading the level, with the corresponding level number, and
     * returns a boolean value that confirms if the level has been correctly created or not.
     *
     * @param levelNumb - Level number to the loaded
     * @return - boolean status informing if the level has been correctly loaded
     * @throws Loader.LevelFormatException - Generates an exception if the information that
     *                                      has been read, isn't the expected format.
     *
     */
    private boolean initLevel(int levelNumb) throws Loader.LevelFormatException {
        level = loader.load(levelNumb);
        if (level == null) return endGame = true;
        return false;
    }

    /**
     * Obtains the size of the map of elements and initializes the array of positions
     * in the class position.
     * Creates a new arena with the same size of the map of elements.
     */
    private void initArena() {
        int lines = level.charMap.length;
        int cols = level.charMap[0].length;
        Position.init(lines, cols);
        arena = new Arena(lines, cols);
    }

    /**
     * Creates the next level by receiving the level
     *
     * @param i - Receives the next level number.
     * @return - boolean status informing if the level has been correctly loaded
     * @throws Loader.LevelFormatException - Generates an exception if the information that
     *                                       has been read, isn't the expected format.
     */
    public boolean nextLevel(int i) throws Loader.LevelFormatException {
        if (initLevel(i)) return true;
        initArena();
        return false;
    }

    /**
     * Obtains the height of the arena area.
     *
     * @return - The value with the height of the arena.
     */
    public int getLines() { return arena.getHeight(); }

    /**
     * Obtains the width of the arena.
     *
     * @return - The value with the width of the arena.
     */
    public int getCols() { return arena.getWidth(); }

    /**
     * Reads the contents of the level array, and creates the elements accordingly.
     */
    public void startLevel() {
        numVirus = 0;
        for (int i = 0; i < level.charMap.length; i++) {
            for (int j = 0; j < level.charMap[0].length; j++) {
                char type = level.charMap[i][j];
                switch (type) {
                    case '@' :
                        nurse = new Nurse(i, j, this);
                        break;
                    case 'X' :
                        new Wall(i, j, this);
                        break;
                    case '*' :
                        new Virus(i, j, this);
                        numVirus++;
                        break;
                    case 'V' :
                        new Trash(i, j, this);
                        break;
                }
            }
        }
    }

    /**
     * Return the current number of virus ingame.
     *
     * @return - The current number of virus ingame.
     */
    public int getNumVirus() {
        return numVirus;
    }

    /**
     * Sets the number of virus ingame.
     * Used when a game starts, or has been reset.
     *
     * @param n - The number of virus to be inserted ingame.
     */
    public void setNumVirus(int n) {
        numVirus = n;
        ctrlListener.valueVirus(n);
    }

    /**
     * Requests the nurse to move to a determined direction, but, only after the verification if
     * the nurse isn't dead, or all virus has been killed.
     *
     * @param dir - Enumeration of the direction to move.
     */
    public void moveNurse(Dir dir) {
        if (nurse.isDead() || numVirus == 0) return;
        currDir = dir;
        nurse.move(dir);
    }

    /**
     * Verifies if the element is moving against something that isn't movable or is
     * colliding against a movable element, then decide if there is going to exist
     * movement for the element.
     * If movement is possible, maintains the new position.
     * If isn't possible, sets the old position.
     *
     * @param old - Previous position.
     * @param element - Element type.
     */
    void movedElement(Position old, Element element) {
        Element e = arena.get(element.position);
        if (e!=element && (e==null || e.elementColision(old) && e.isMove(old))) {
            arena.resetPosition(old);
            arena.set(element);
            listener.elementMoved(element, old);
        } else {
            element.position = old;
        }
    }

    /**
     * Allows to obtain the list of active movable elements in the current arena.
     *
     * @return List of movable elements
     */
    public LinkedList<Element> getRemoveMovables() {
        return arena.removeMovables();
    }

    /**
     * Allows an movable object to be moved down.
     * Simulates gravity if there isn't any object ot stop it.
     *
     * @param e - element type.
     */
    public void moveMovable(Element e) {
        currDir = Dir.DOWN;
        e.move(currDir);
    }

    /**
     * Get the current information for the nurse
     *
     * @return Nurse type.
     */
    public Nurse getNurse() {
        return nurse;
    }

    /**
     * Requests an position update to be made for an element.
     * @param pos - Position information
     */
    void updateElement(Position pos) {
        listener.elementUpdate(pos);
    }

    /**
     * This method allows to create new elements. It requests the arena to create, and
     * also updates the listener, that a new element is being created, and it needs to be drawn.
     *
     * @param element - Receives an element type.
     */
    void createElement(Element element) {
        arena.set(element);
        listener.elementCreated(element);
    }

    /**
     *  Allows to destroy an elements in the current arena. Also, requests the view to
     *  update and destroy the element.
     *  Decreases the number of virus alive ingame.
     *
     * @param p - Current position of the element.
     */
    void destroyElement(Position p) {
        listener.elementDestroy(p);
        --numVirus;
        updateValueVirus();
    }

    /**
     * Requests and update to the view, upon the number of virus.
     */
    void updateValueVirus() {
        ctrlListener.valueVirus(numVirus);
    }

    /**
     * Save bundle information, when an higher priority activity enter or when the screen
     * is rotate, for example.
     *
     * @param dos - Receives the OutputStream to be saved.
     * @throws IOException - Generic class of exceptions for input/output information.
     */
    public void saveBundle(DataOutputStream dos) throws IOException {
        save(dos);
    }

    /**
     * Load bundle information, after the return of the activity, or the screen rotation ends.
     *
     * @param dis Receives the InputStream to be loaded
     * @throws IOException - Generic class of exceptions for input/output information.
     * @throws ClassNotFoundException - Generates exception if in a search for a class using
     *                                  it's name, the same is not found, through reflection.
     * @throws InstantiationException - Generates exception if there is a request to instantiate
     *                                  an object through reflection , and it is not possible to
     *                                  do so. The cause are due to the class has no nullary
     *                                  constructor, or represent and class that isn't instantiable.
     *
     * @throws IllegalAccessException - If an application tries to reflectively create an instance
     *                                  set, get or invoke a method, but the currently executing
 *                                      method don't have access to the specified class, method
 *                                      or field.
     * @throws Loader.LevelFormatException - Generates an exception if the information that
*    *                                       has been read, isn't the expected format.
     */
    public void loadBundle(DataInputStream dis) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, Loader.LevelFormatException {
        load(dis);
    }

    /**
     * Creates an OutputStream, and saves it to a file.
     * @param dos - Receives the OutputStream to be written
     *
     * @throws IOException - Generic class of exceptions for input/output information.
     */
    public void save(DataOutputStream dos) throws IOException {
        for (Element e : arena) {
            dos.writeUTF(e.getClass().getSimpleName());
            e.save(dos);
        }
        dos.writeUTF("END");
    }

    /**
     * Receives an InputStream, and loads it from a file.
     *
     * @param dis - Receives the InputStream to be loaded.
     * @throws IOException - - Generic class of exceptions for input/output information.
     * @throws ClassNotFoundException - Generates exception if in a search for a class using
     *                                  it's name, the same is not found, through reflection.
     * @throws InstantiationException - Generates exception if there is a request to instantiate
     *                                  an object through reflection , and it is not possible to
     *                                  do so. The cause are due to the class has no nullary
     *                                  constructor, or represent and class that isn't instantiable.
     * @throws IllegalAccessException - If an application tries to reflectively create an instance
     *                                  set, get or invoke a method, but the currently executing
     *                                  method don't have access to the specified class, method
     *                                  or field.
     */
    public void load(DataInputStream dis) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        for(;;) {
            String name = dis.readUTF();
            if (name.equals("END")) break;
            Class cls = Class.forName("pt.isel.poo.covid.model."+name);
            Element e = (Element) cls.newInstance();
            e.model = this;
            e.load(dis);
            createElement(e);
            if (Nurse.class == cls) {
                nurse = (Nurse) e;
                nurseDead();
            }
        }
    }

    @NonNull
    @Override
    public Iterator<Element> iterator() {
        return arena.iterator();
    }

    private GameListener listener;

    /**
     * Receives information about the listener and assign's it to the field listener.
     *
     * @param listener - Listener Object
     */
    public void addListener(GameListener listener) {
        this.listener = listener;
    }

    /**
     * Verifies if the nurse is dead, and, in case of confirmation, warns the view, through
     * the listener, to update the view.
     */
    public void nurseDead() {
        boolean a = nurse.isDead();
        listener.nurseDead(a);
    }

    /**
     * Requests an arena reset.
     */
    public void reset() {
        arena.reset();
    }

    /**
     * Requests an update to a listener, for the number of virus.
     */
    public interface Listener {
        void valueVirus(int value);
    }


    private Listener ctrlListener = null;

    /**
     * Accepts the request to create a new listener.
     *
     * @param l - Listener object reference.
     */
    public void setListener(Listener l) {
        ctrlListener = l;
    }

}
