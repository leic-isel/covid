package pt.isel.poo.covid.model;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Trash extends Element {
    Trash(int l, int c, CovidModel m) {
        super(l, c, m);
    }

    /**
     * Required constructor if there is the need to use reflection on this class.
     */
    Trash() {}

    /**
     * Verifies if there was a collision between thrash and some other element,
     * movable.
     * @param old - Position reference
     * @return - true if there was a collision with another element.
     */
    @Override
    boolean elementColision(Position old) {
        model.arena.get(old).trashCollision(old);
        return false;
    }

    /***
     * This method always return false for this class, since this type of object ins't
     * movable.
     *
     * @return - Returns false.
     */
    @Override
    boolean isMovable() {
        return false;
    }

    /**
     * It allows to verify if an element has  collided with the trash can.
     * The element that collied with the trash can it will take the respective
     * action accordingly to the rules set at it's own class.
     *
     * @param p - Future position.
     */
    @Override
    void trashCollision(Position p) { }

    /**
     * Verifies if an element has been moved, and returns true if there was movement.
     *
     * @param old - Position type referring to the old position
     * @return - Return true if movement was successful
     */
    @Override
    boolean isMove(Position old) {
        return super.isMove(old);
    }

    /**
     * Saves the current state of the element.
     *
     * @param dos - Stream of data
     * @throws IOException - Generic class of exceptions for input/output information.
     */
    @Override
    void save(DataOutputStream dos) throws IOException {
        super.save(dos);
    }

    /**
     * Reads from the output stream information regarding the position of an element.
     *
     * @param dis - Stream of data
     * @throws IOException - Generic class of exceptions for input/output information.
     */
    @Override
    void load(DataInputStream dis) throws IOException {
        super.load(dis);
    }

}
