package pt.isel.poo.tile;

import pt.isel.poo.covid.model.Loader;

/**
 *
 */
public interface OnBeatListener {
    void onBeat(long beat, long time) throws Loader.LevelFormatException;
}
