package pt.isel.poo.covid;

import android.app.Activity;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Scanner;

import pt.isel.poo.covid.model.CovidModel;
import pt.isel.poo.covid.model.Dir;
import pt.isel.poo.covid.model.Element;
import pt.isel.poo.covid.model.Loader;
import pt.isel.poo.covid.view.CovidView;
import pt.isel.poo.tile.OnBeatListener;
import pt.isel.poo.tile.TilePanel;

public class CovidController extends Activity {

    private final String FILESAVE = "Covid.txt";
    private final String FILELEVEL = "covid_levels.txt";
    private AssetManager manager;
    private int numbLevel = 1;
    private boolean end;
    private final int MOVE_PERIOD = 300;

    //Buttons
    Button save, load, ok;
    ImageButton left, right;

    //load
    Scanner loadLevel;

    //View
    CovidView view;
    TilePanel panel;
    TextView levelTV, virusTV, infoTV;
    LinearLayout infoViewLL;

    //model
    CovidModel model;

    @Override
    protected void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.activity_main);

        loadViewsById();

        save.setOnClickListener(ctrlBtnOnClickListener);
        load.setOnClickListener(ctrlBtnOnClickListener);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(end) {
                    finish();
                }else {
                    try {
                        onClickOK();
                    } catch (Loader.LevelFormatException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restartTimeBase();
                moveNurse(Dir.LEFT);
            }
        });

        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restartTimeBase();
                moveNurse(Dir.RIGHT);
            }
        });

        manager = getResources().getAssets();
        try {
            loadLevel = new Scanner(manager.open(FILELEVEL));
            model = new CovidModel(loadLevel, numbLevel);
        } catch (IOException | Loader.LevelFormatException e) { e.printStackTrace(); }

        view = new CovidView(this, model, panel);
        if (state == null) {
            model.startLevel();
        }

        add(model.getNumVirus(), virusTV);
        add(numbLevel, levelTV);
        model.setListener(new CovidModel.Listener() {
            @Override
            public void valueVirus(int value) {
                reset(R.string.numvirus, virusTV);
                add(value, virusTV);
            }
        });

        setTimeBase();

    }

    //Initialize the fields of CovidController by recurring to the preset value in xml layout.
    private void loadViewsById() {
        panel = findViewById(R.id.tilepanel);
        levelTV = findViewById(R.id.textLevel);
        virusTV = findViewById(R.id.textVirus);
        infoTV = findViewById(R.id.info);
        infoViewLL = findViewById(R.id.infoView);
        save = findViewById(R.id.save);
        load = findViewById(R.id.load);
        ok = findViewById(R.id.ok);
        left = findViewById(R.id.left);
        right = findViewById(R.id.right);
    }

    // Save and Load buttons listener.
    View.OnClickListener ctrlBtnOnClickListener = (v) -> {
        if (v == save) {
            try {
                save();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if (v == load) {
            try {
                load();
            } catch (FileNotFoundException e) {
                e.printStackTrace();

                msg(getString(R.string.filenotfound));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * This method allows to change the text in a TextView.
     *
     * @param value - An integer number.
     * @param view - The view that is goig to e changed
     */
    private void add(int value, TextView view){
        view.setText(view.getText().toString()+value);
    }


    /**
     * Reset the current text in a textview and sets it to its default value or any other.
     *
     * @param s - Integer value.
     * @param v - Textview to be changed
     */
    private void reset(int s, TextView v) {
        v.setText(s);
    }

    /**
     * Update the view showing the level number.
     */
    private void updateNumLevel(){
        reset(R.string.level, levelTV);
        add(numbLevel, levelTV);
    }

    /**
     * Once this button is pushed, it starts the actions that will be responsible to
     * removed the previous contents and draws a the new level.
     *
     * @throws Loader.LevelFormatException - Generates an exception if the information that
     * *    *                                has been read, isn't the expected format.
     * @throws IOException - Generic class of exceptions for input/output information.
     */
    private void onClickOK() throws Loader.LevelFormatException, IOException {
        model.setLoader(new Scanner(manager.open(FILELEVEL)));
        if (model.nextLevel((numbLevel+=1))) return;
        view.initTilePanel(panel, model);
        view.reset();
        model.startLevel();
        model.setNumVirus(model.getNumVirus());
        updateNumLevel();
        infoViewLL.setVisibility(LinearLayout.INVISIBLE);
    }

    /**
     * This method sets a time period to update the view, and allowing to simulate gravity
     * if nothing is bellow an element.
     */
    private void setTimeBase() {
        panel.setHeartbeatListener(MOVE_PERIOD, new OnBeatListener() {
            @Override
            public void onBeat(long beat, long time) throws Loader.LevelFormatException {
                LinkedList<Element> movables = model.getRemoveMovables();
                //if (!model.getNurse().isDead()) // para fazer debug
                for (Element e : movables) {
                    model.moveMovable(e);
                }
                end = endConditions();
            }
        });
    }

    /**
     * Verifies if any end condition is true.
     * When an end condition is archived then a layout will be made visible with a message and
     * the ok button.
     *
     * @return - The state of the game.
     */
    private boolean endConditions() {
        if (model.getEndGame()) {
            infoTV.setText(R.string.wingame);
            return true;
        } else if (model.getNurse().isDead()) {
            infoTV.setText(R.string.gameover);
            infoViewLL.setVisibility(LinearLayout.VISIBLE);
            return true;
        } else if (model.getNumVirus() == 0) {
            infoTV.setText(R.string.levelcomplete);
            infoViewLL.setVisibility(LinearLayout.VISIBLE);
            return false;
        }
        return true;
    }

    /**
     * Stop the current timebase and set a new one.
     */
    private void restartTimeBase() {
        panel.removeHeartbeatListener();
        setTimeBase();
    }

    /**
     * Responsible to move the accordingly to the direction received.
     * @param dir - Direction to move
     */
    private void moveNurse(Dir dir) {
        model.moveNurse(dir);
    }

    private void save() throws IOException {
        PrintWriter pw = new PrintWriter(
                new OutputStreamWriter(openFileOutput(
                        FILESAVE,
                        MODE_PRIVATE)
                )
        );
        ByteArrayOutputStream baos;
        try (DataOutputStream dos = new DataOutputStream(baos = new ByteArrayOutputStream())) {
            model.save(dos);
        }
        byte[] b = baos.toByteArray();
        pw.println(b.length);
        pw.println(numbLevel);
        pw.println(model.getNumVirus());
        pw.println(model.getEndGame());
        for (int i = 0; i < b.length; i++) {
            pw.println(b[i]);
        }
        pw.close();
    }

    private void load() throws IOException {
        view.reset();
        model.reset();
        Scanner in = new Scanner(openFileInput(FILESAVE));
        byte[] b = new byte[Integer.parseInt(in.nextLine())];
        numbLevel = Integer.parseInt(in.nextLine());
        updateNumLevel();
        model.setNumVirus(Integer.parseInt(in.nextLine()));
        model.setEndGame(in.nextLine().equals("true"));
        for (int i = 0; i < b.length; i++) {
            b[i] = (byte) Integer.parseInt(in.nextLine());
        }

        try (DataInputStream dis = new DataInputStream(new ByteArrayInputStream(b))) {
            model.load(dis);
        } catch (IllegalAccessException | InstantiationException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        infoViewLL.setVisibility(LinearLayout.INVISIBLE);
    }

    /**
     * Allows to save the current state of the game, when an higher priority activity is
     * called, or when the screen is rotated.
     * @param state - Bundle state
     */
    @Override
    protected void onSaveInstanceState(@NonNull Bundle state) {
        super.onSaveInstanceState(state);
        ByteArrayOutputStream baos;
        try (DataOutputStream dos =
                     new DataOutputStream(baos =
                             new ByteArrayOutputStream())) {
            state.putInt("LEVEL", numbLevel);
            state.putInt("VIRUS", model.getNumVirus());
            model.saveBundle(dos);
        } catch (IOException e) { e.printStackTrace(); return; }
        state.putByteArray("MODEL", baos.toByteArray());
    }

    /**
     * Allows ro restore the previous saved state when the application wa interrupted.
     *
     * @param state - Bundle state.
     */
    @Override
    protected void onRestoreInstanceState(@NonNull Bundle state) {
        super.onRestoreInstanceState(state);
        try (DataInputStream dis =
                     new DataInputStream(
                             new ByteArrayInputStream(state.getByteArray("MODEL")))) {
            numbLevel = state.getInt("LEVEL");
            updateNumLevel();
            model.setNumVirus(state.getInt("VIRUS"));
            model.loadBundle(dis);
        } catch (IOException | IllegalAccessException | InstantiationException | ClassNotFoundException | Loader.LevelFormatException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method that make messages appear on the screen, when an important information
     * is required to be shown
     *
     * @param msg - String
     */
    public void msg (String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }
}