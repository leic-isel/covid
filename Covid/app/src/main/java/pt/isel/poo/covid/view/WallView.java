package pt.isel.poo.covid.view;

import pt.isel.poo.covid.CovidController;
import pt.isel.poo.covid.R;
import pt.isel.poo.tile.Img;

public class WallView extends ElementView {

    /**
     * Construction  of a wall object
     * @param ctx - Context of the application.
     */
    public WallView(CovidController ctx) {
        img = new Img(ctx, R.drawable.wall);
    }
}
