package pt.isel.poo.covid.view;

import android.graphics.Canvas;
import android.graphics.Paint;
import java.util.HashMap;
import pt.isel.poo.covid.CovidController;
import pt.isel.poo.covid.model.Element;
import pt.isel.poo.covid.model.Nurse;
import pt.isel.poo.covid.model.Trash;
import pt.isel.poo.covid.model.Virus;
import pt.isel.poo.covid.model.Wall;
import pt.isel.poo.tile.Img;
import pt.isel.poo.tile.Tile;

public abstract class ElementView implements Tile {
    Paint p = new Paint();
    Img img;

    /**
     * HashMap of Elements and the respectively tiles.
     */
    static HashMap<Class, Tile> modelToView = new HashMap<>();

    /**
     * Creates the HaspMap of the Elements by using reflection to obtain the information about
     * the elements.
     *
     * @param ctx - Context for the application.
     */
    public static void createHashMap(CovidController ctx) {
        modelToView.put(Nurse.class, new NurseView(ctx));
        modelToView.put(Trash.class, new TrashView(ctx));
        modelToView.put(Virus.class, new VirusView(ctx));
        modelToView.put(Wall.class, new WallView(ctx));
    }

    /**
     * Allows to get the object that is instantiated.
     * @param e - Element object reference.
     * @return
     */
    public static Tile getInstance(Element e) {
        Tile tile = modelToView.get(e.getClass());
        return tile;
    }

    /**
     * Generic method to draw the elements in the screen.
     * @param canvas To draw the tile
     * @param side The width of tile in pixels
     */
    @Override
    public void draw(Canvas canvas, int side) {
        img.draw(canvas, side, side, p);
    }

    @Override
    public boolean setSelect(boolean selected) {
        return false;
    }

}