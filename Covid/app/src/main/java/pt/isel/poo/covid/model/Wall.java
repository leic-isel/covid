package pt.isel.poo.covid.model;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Wall extends Element {

    Wall(int l, int c, CovidModel m) {
        super(l, c, m);
    }

    /**
     * Required constructor if there is the need to use reflection on this class.
     */
    Wall() {}

    /**
     * Verifies if there was a collision between wall and some other element,
     * movable.
     * @param old - Position reference
     * @return - true if there was a collision with another element.
     */
    @Override
    boolean elementColision(Position old) {
        return false;
    }

    //Always false.
    @Override
    boolean isMovable() {
        return false;
    }

    // Not used in this class
    @Override
    void trashCollision(Position p) { }

    /**
     * Verifies if an element has been moved, and returns true if there was movement
     *
     * @param old - Position type referring to the old position
     * @return - Return true if movement was successful
     */
    @Override
    boolean isMove(Position old) {
        return super.isMove(old);
    }

    /**
     * Saves the current state of the element.
     *
     * @param dos - Stream of data
     * @throws IOException - Generic class of exceptions for input/output information.
     */
    @Override
    void save(DataOutputStream dos) throws IOException {
        super.save(dos);
    }

    /**
     * Reads from the output stream information regarding the position of an element.
     *
     * @param dis - Stream of data
     * @throws IOException - Generic class of exceptions for input/output information.
     */
    @Override
    void load(DataInputStream dis) throws IOException {
        super.load(dis);
    }

}
