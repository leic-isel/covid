package pt.isel.poo.covid.model;

/**
 * Implementing this interface allows to warn about updates in the state of objects.
 *
 */
public interface GameListener {
    void elementCreated(Element elem);
    void elementMoved(Element elem, Position pos);
    void elementDestroy(Position pos);
    void elementUpdate(Position pos);
    void nurseDead(boolean loser);
}
