package pt.isel.poo.covid.view;

import pt.isel.poo.covid.CovidController;
import pt.isel.poo.covid.R;
import pt.isel.poo.tile.Img;

public class VirusView extends ElementView {

    /**
     * Construction of a virus object
     * @param ctx - Context of the application.
     */
    public VirusView(CovidController ctx) {
        img = new Img(ctx, R.drawable.virus);
    }
}
